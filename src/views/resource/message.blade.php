@if(session('message'))
<div class="vault__notification notification is-{{ session('type') }}">
    <button class="delete"></button>
    {{ session('message') }}
</div>
@endif

@push('scripts')
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', () => {
            (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
                $notification = $delete.parentNode;
                $delete.addEventListener('click', () => {
                    $notification.parentNode.removeChild($notification);
                });
            });
        });
    </script>
@endpush