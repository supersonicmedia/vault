@if(files($slug))
<section class="hero is-light" id="files">
    <div class="hero-body">
        <div class="container">
            <h2 class="title">{{ __('vault::messages.title-files') }}</h2>
            <form action="{{ route('admin.model.update', [$slug, $item->id]) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="columns is-multiline is-mobile">
                    @if($item->files)
                    @foreach($item->files as $file)
                        <div class="column is-one-quarter-fullhd is-one-third-desktop is-half-tablet">
                            <div class="box">
                                @if(strpos($file['mime_type'], 'audio') !== false)
                                    <div>
                                        <audio class="player_plyr" controls>
                                            <source src="{{ $file['url'] }}" type="audio/mp3" />
                                        </audio>
                                    </div>
                                @endif
                                <div style="overflow: hidden;">
                                    <a class="button is-block" download href="{{ $file['url'] }}">{{ $file['name'] }}</a>
                                </div>
                                <br>
                                <button onclick="return confirm('{{ __('vault::messages.confirm') }}')" value="{{ $file['id'] }}" type="submit" value="yes" name="delete-files" class="button is-fullwidth">
                                    <span class="icon"><i class="fas fa-trash"></i></span>
                                    <span>{{ __('vault::messages.delete-file') }}</span>
                                </button>
                            </div>
                        </div>
                    @endforeach
                    @endif
                    <div class="column is-one-quarter-fullhd is-one-third-desktop is-half-tablet">
                        <div class="box">
                            <div class="file has-name is-boxed is-fullwidth">
                                <label class="file-label">
                                    <input class="file-input" type="file" name="files[]" data-multiple-caption="{count} {{ __('vault::messages.selected-files') }}" multiple>
                                    <span class="file-cta">
                                        <span class="file-icon">
                                            <i class="fas fa-upload"></i>
                                        </span>
                                        <span class="file-label has-text-centered">
                                            {{ __('vault::messages.choose-files') }}
                                        </span>
                                    </span>
                                    <span class="file-name">...</span>
                                </label>
                            </div>
                            <br>

                            <button type="submit" name="button" class="button is-fullwidth is-primary">{{ __('vault::messages.add') }}</button>
                            @if(!gallery($slug))
                                @push('scripts')
                                    <script>
                                        $( '.file-input' ).each( function()
                                        {
                                            var $input	 = $( this ),
                                                $label	 = $input.parents('.file-label').find( '.file-name' ),
                                                labelVal = $label.html();

                                            $input.on( 'change', function( e )
                                            {
                                                var fileName = '';

                                                if( this.files && this.files.length > 1 )
                                                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                                                else if( e.target.value )
                                                    fileName = e.target.value.split( '\\' ).pop();

                                                if( fileName )
                                                    $label.html( fileName );
                                                else
                                                    $label.html( labelVal );
                                            });

                                            // Firefox bug fix
                                            $input
                                                .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
                                                .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
                                        });
                                    </script>
                                @endpush
                            @endif
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endif

@push('meta')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/plyr/3.5.10/plyr.css">
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/plyr/3.5.10/plyr.min.js"></script>
    <script type="text/javascript">
        const players = Array.from(document.querySelectorAll('.player_plyr')).map(player => new Plyr(player));
        players.forEach(function(instance,index) {
            instance.on('play',function(){
                players.forEach(function(instance1,index1){
                    if(instance != instance1){
                        instance1.pause();
                    }
                });
            });
        });
    </script>
@endpush
