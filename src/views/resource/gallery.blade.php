@if(gallery($slug))
<section class="hero is-light" id="gallery">
    <div class="hero-body">
        <div class="container">
            <h2 class="title">{{ __('vault::messages.title-gallery') }}</h2>
                <div class="columns is-multiline is-mobile">
                    @if($item->gallery)
                    @foreach($item->gallery as $photo)
                        <div class="column is-one-quarter-fullhd is-one-third-desktop is-half-tablet">
                            <form action="{{ route('admin.model.update', [$slug, $item->id]) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box">
                                <div style="height: 118px; overflow: hidden;" class="field">
                                    <a class="image" data-lity href="{{ $photo['large'] }}">
                                        <img src="{{ $photo['medium'] }}" alt="Image">
                                    </a>
                                </div>
                                <div class="columns field" style="margin-bottom: 0;">
                                    <div class="column">
                                        <div class="select is-fullwidth">
                                            <select name="orientation[{{ $photo['id'] }}]">
                                                <option @if($photo['orientation'] == 'horizontal') selected @endif value="horizontal">{{ __('vault::messages.horizontal') }}</option>
                                                <option @if($photo['orientation'] == 'vertical') selected @endif value="vertical">{{ __('vault::messages.vertical') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="column">
                                        <button type="submit" name="button" class="button is-fullwidth">{{ __('vault::messages.save') }}</button>
                                    </div>
                                </div>
                                <button onclick="return confirm('{{ __('vault::messages.confirm') }}')" value="{{ $photo['id'] }}" type="submit" value="yes" name="delete-gallery" class="button is-fullwidth">
                                    <span class="icon"><i class="fas fa-trash"></i></span>
                                    <span>{{ __('vault::messages.delete-image') }}</span>
                                </button>
                            </div>
                        </form>
                        </div>
                    @endforeach
                    @endif
                    <div class="column is-one-quarter-fullhd is-one-third-desktop is-half-tablet">
                        <form action="{{ route('admin.model.update', [$slug, $item->id]) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box">
                                <div class="file has-name is-boxed is-fullwidth">
                                    <label class="file-label">
                                        <input class="file-input" type="file" name="gallery[]" data-multiple-caption="{count} {{ __('vault::messages.selected-files') }}" multiple>
                                        <span class="file-cta">
                                            <span class="file-icon" style="margin-right: 0;">
                                                <i class="fas fa-upload"></i>
                                            </span>
                                            <span class="file-label has-text-centered">
                                                {{ __('vault::messages.choose-files') }}
                                            </span>
                                        </span>
                                        <span class="file-name">...</span>
                                    </label>
                                </div>
                                <br>
                                
                                <button type="submit" name="button" class="button is-fullwidth is-primary">{{ __('vault::messages.add') }}</button>
                                @push('scripts')
                                    <script>
                                        $( '.file-input' ).each( function()
                                        {
                                            var $input	 = $( this ),
                                                $label	 = $input.parents('.file-label').find( '.file-name' ),
                                                labelVal = $label.html();

                                            $input.on( 'change', function( e )
                                            {
                                                var fileName = '';

                                                if( this.files && this.files.length > 1 )
                                                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                                                else if( e.target.value )
                                                    fileName = e.target.value.split( '\\' ).pop();

                                                if( fileName )
                                                    $label.html( fileName );
                                                else
                                                    $label.html( labelVal );
                                            });

                                            // Firefox bug fix
                                            $input
                                                .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
                                                .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
                                        });
                                    </script>
                                @endpush
                            </div>
                        </form>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endif