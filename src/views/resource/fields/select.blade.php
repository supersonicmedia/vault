@php($relation_model = entity($slug)['fields'][$field]['model'])
@php($relation_field = entity($slug)['fields'][$field]['field'])
<div class="field">
    <label class="label">{{ ___('fields.' . $field) }}</label>
    <div class="select">
        <select name="{{ $field }}">
            <option value="">-</option>
            @foreach(model($relation_model)::all() as $additional)
            @php($current_value = old($field, isset($item) ? $item->getOriginal($field) : ''))
            <option {{ ($current_value == $additional->id) ? 'selected' : '' }} 
                value="{{ $additional->id }}">#{{ $additional->id }} {{ $additional->{$relation_field} }}</option>
            @endforeach
        </select>
    </div>
    @if($errors->has($field))
        <p class="help is-danger">{{ $errors->first($field) }}</p>
    @endif
</div>