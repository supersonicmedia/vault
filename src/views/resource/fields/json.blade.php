@php(isset($item) ? $json = json_decode($item->getOriginal($field), true) : null)
@php($json[''] = '')
<div class="field">
    <label class="label">{{ ___('fields.' . $field) }}</label>
    @foreach ($json as $label => $value)
    <div class="columns">
        <div class="column">
            <input type="text" name="{{ $field }}[label][]" value="{{ old($field) ? old($field)['label'][0] : $label }}" class="input" placeholder="{{ ___('fields.' . $field) }}">
        </div>
        <div class="column">
            <input type="text" name="{{ $field }}[value][]" value="{{ old($field) ? old($field)['value'][0] : $value }}" class="input" placeholder="{{ ___('fields.' . $field) }}">
        </div>
    </div>
    @endforeach
</div>