<div class="field">    
    <div class="columns is-mobile">
        <div class="column">
            <label class="label">{{ ___('fields.' . $field) }}</label>
            <input class="input {{ $errors->has($field) ? 'is-danger' : '' }}" 
            type="date" 
            name="{{ $field }}" 
            placeholder="{{ ___('fields.' . $field) }}" 
            data-start-date="{{ old($field, isset($item) ? $item->getOriginal($field) : '') }}" 
            {{ (!$param[$mode] || $mode === 'show') ? 'disabled' : '' }}>
        </div>
        <div class="column is-narrow">
            <label class="label">{{ ___('messages.hours') }}</label>
            <span class="button add-hour">
                <span class="icon is-small">
                    <i class="fas fa-plus"></i>
                </span>
            </span>
            <span class="button sub-hour">
                <span class="icon is-small">
                    <i class="fas fa-minus"></i>
                </span>
            </span>
        </div>
    </div>
    @if($errors->has($field))
        <p class="help is-danger">{{ $errors->first($field) }}</p>
    @endif
</div>