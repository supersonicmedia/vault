<div class="field">
    <label class="label">{{ ___('fields.' . $field) }}</label>
    <textarea class="wysiwyg textarea {{ $errors->has($field) ? 'is-danger' : '' }}"
        name="{{ $field }}"
        placeholder="{{ ___('fields.' . $field) }}"
        {{ (!$param[$mode] || $mode === 'show') ? 'disabled' : '' }}>
        {{ old($field, isset($item) ? $item->getOriginal($field) : '') }}
    </textarea>
    @if($errors->has($field))
        <p class="help is-danger">{{ $errors->first($field) }}</p>
    @endif
</div>
