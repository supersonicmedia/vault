<div class="field">
    <div class="pretty p-switch" style="cursor: pointer;">
        <input id="{{ "checkbox-$field" }}"
        type="checkbox"
        name="{{ $field }}"
        {{ (!$param[$mode] || $mode === 'show') ? 'disabled' : '' }}
        {{ (old($field) == 'on' || (isset($item) && $item->getOriginal($field))) ? 'checked' : '' }} />
        <div class="state">
            <label style="cursor: pointer;" for="{{ "checkbox-$field" }}">{!! ___('fields.' . $field) !!}</label>
        </div>
    </div>
</div>
