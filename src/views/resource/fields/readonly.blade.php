<div class="field">
    <label class="label">
        {{ ___('fields.' . $field) }}
    </label>
    <input name="{{ $field }}" class="input {{ $errors->has($field) ? 'is-danger' : '' }}" 
    type="text" value="{{ isset($item) ? $item->getOriginal($field) : '' }}" readonly style="background-color: #f1f1f1; pointer-events: none;">
</div>