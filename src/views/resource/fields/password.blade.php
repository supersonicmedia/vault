<div class="field">
    <label class="label">{{ ___('fields.' . $field) }}</label>
    <input class="input {{ $errors->has($field) ? 'is-danger' : '' }}" 
    type="password" 
    name="{{ $field }}" 
    placeholder="{{ ___('fields.' . $field) }}" 
    value="{{ old($field, (isset($item) ? $item->getOriginal($field) : '')) }}" 
    {{ (!$param[$mode] || $mode === 'show') ? 'disabled' : '' }}>
    @if($errors->has($field))
        <p class="help is-danger">{{ $errors->first($field) }}</p>
    @endif
</div>