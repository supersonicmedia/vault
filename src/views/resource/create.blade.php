@extends('vault::layout')

@section('content')
    
    @include('vault::resource.hero', [$slug])
    
    <div class="section" style="overflow: hidden;">
        <div class="container">
            
            <form action="{{ route('admin.model.store', [$slug]) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                
                @foreach($fields as $field => $param)
                    
                    @if($param['create'])
                        @include('vault::resource.fields.' . $param['type'], ['mode' => 'create', 'field' => $field, 'param' => $param, 'errors' => $errors, 'slug' => $slug])
                    @endif
                    
                @endforeach
                
                <div class="field">
                    <div class="control">
                        <button type="submit" class="button is-link">
                            <span class="icon"><i class="fas fa-save"></i></span>
                            <span>{{ __('vault::messages.save') }}</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
@endsection