@extends('vault::layout')

@section('content')

    <section class="hero {{ colour($slug) }} is-bold">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">{{ config("vault.models.$slug.title", ucfirst($slug)) }}</h1>
                <div class="container">
                    <div class="columns is-vcentered is-mobile is-multiline">
                        <div class="column is-half-mobile">
                            <form action="{{ route('admin.model.index', [$slug]) }}">
                                <div class="select {{ colour($slug) }} is-inverted is-outlined is-fullwidth">
                                    <select name="sort" onchange="$(this).parents('form').submit()">
                                        <option value="select">{{ __('vault::messages.sort') }}</option>
                                        @foreach($fields as $field => $param)
                                        <option {{ request()->sort == "$field-asc" ? 'selected' : '' }} value="{{ $field }}-asc">{!! ___('fields.' . $field) !!} &uarr;</option>
                                        <option {{ request()->sort == "$field-desc" ? 'selected' : '' }} value="{{ $field }}-desc">{!! ___('fields.' . $field) !!} &darr;</option>
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                        </div>

                        <div class="column is-half-mobile">
                            @if(creatable($slug))
                                <a class="button {{ colour($slug) }} is-inverted is-outlined is-fullwidth" href="{{ route('admin.model.create', [$slug]) }}">
                                    <span class="icon"><i class="fas fa-plus"></i></span>
                                    <span>{{ __('vault::messages.create') }}</span>
                                </a>
                            @else
                                <div class="button {{ colour($slug) }} is-inverted is-outlined is-fullwidth" disabled>
                                    <span class="icon"><i class="fas fa-plus"></i></span>
                                    <span>{{ __('vault::messages.create') }}</span>
                                </div>
                            @endif
                        </div>
                        <div class="column is-half-mobile">

                            @if($items->count())
                                <a download class="button {{ colour($slug) }} is-inverted is-outlined is-fullwidth" href="{{ route('admin.model.export', [$slug]) }}">
                                    <span class="icon"><i class="fas fa-download"></i></span>
                                    <span>{{ __('vault::messages.export_xls') }}</span>
                                </a>
                            @else
                                <div class="button {{ colour($slug) }} is-inverted is-outlined is-fullwidth" disabled>
                                    <span class="icon"><i class="fas fa-download"></i></span>
                                    <span>{{ __('vault::messages.export_xls') }}</span>
                                </div>
                            @endif

                        </div>
                        @if(import($slug))
                            <form method="post" action="{{ route('admin.model.import', [$slug]) }}" class="column is-half-mobile" enctype="multipart/form-data">
                                @csrf
                                <label for="import" class="button {{ colour($slug) }} is-inverted is-outlined is-fullwidth">
                                    <input id="import" type="file" class="is-sr-only" name="import_file">
                                    <span class="icon"><i class="fas fa-upload"></i></span>
                                    <span>{{ __('vault::messages.import_xls') }}</span>
                                </label>
                            </form>
                            @push('scripts')
                                <script>
                                    $('#import').change(function (event) {
                                        if ($(this).val()) {
                                            $(this).closest('form').submit();
                                        }
                                    });
                                </script>
                            @endpush
                        @else
                            <div class="column is-half-mobile">
                                <a href="" class="button {{ colour($slug) }} is-inverted is-outlined is-fullwidth" disabled>
                                    <span class="icon"><i class="fas fa-upload"></i></span>
                                    <span>{{ __('vault::messages.import_xls') }}</span>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </section>

    @include('vault::resource.message')

    @if($items->count())

        <form action="{{ route('admin.model.actions', [$slug]) }}" method="post">
        @csrf
        @if($actions['enebled'])
            <section style="padding-top: 10px; padding-bottom: 10px;">
                <div class="container">
                    @if($actions['activity'])
                        <button type="submit" name="type" value="activate" class="button">{{ __('vault::messages.activate') }}</button>
                        <button type="submit" name="type" value="deactivate" class="button">{{ __('vault::messages.deactivate') }}</button>
                    @endif
                    @if($actions['delete'])
                        <button type="submit" name="type" value="delete" class="button" onclick="return confirm('{{ __('vault::messages.confirm') }}')">{{ __('vault::messages.delete') }}</button>
                    @endif
                </div>
            </section>
        @endif

        <div class="container">
            <table class="table is-fullwidth is-hoverable">
                <thead>
                    <tr>
                        @if($actions['enebled'])
                            <th>
                                <input type="checkbox" class="actions__checkbox_main">
                            </th>
                            @push('scripts')
                                <script>
                                    $(document).ready(function() {
                                        $('.actions__checkbox_main').change(function (e) {
                                            $('.actions__checkbox').prop('checked', this.checked);
                                        });
                                    });
                                </script>
                            @endpush
                        @endif
                        <th><abbr title="ID">ID</abbr></th>
                        @foreach($fields as $field => $param)
                            @if(isset($param['filter']))
                                <th class="{{ empty($param['mobile']) ? 'is-hidden-mobile' : '' }}">
                                    <form action="{{ route('admin.model.index', [$slug]) }}">
                                        <div class="select {{ colour($slug) }} is-inverted is-outlined is-fullwidth">
                                            <select name="filter[{{ $field }}]" onchange="$(this).parents('form').submit()">
                                                <option value="clear_filter">{!! ___('fields.' . $field) !!}</option>
                                                @if(isset(entity($slug)['fields'][$field]['model']))
                                                    @php($relation_model = entity($slug)['fields'][$field]['model'])
                                                    @php($relation_field = entity($slug)['fields'][$field]['field'])
                                                    @foreach(model($relation_model)::all() as $additional)
                                                        <option {{ request()->input('filter.' . $field) == $additional->id ? 'selected' : '' }} value="{{ $additional->id }}">#{{ $additional->id }} {{ $additional->{$relation_field} }}</option>
                                                    @endforeach
                                                @else
                                                    @foreach(model(entity($slug)['name'])::all()->unique($field)->pluck($field) as $additional)
                                                        <option {{ request()->input('filter.' . $field) == $additional ? 'selected' : '' }} value="{{ $additional }}">{{ $additional }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </form>
                                </th>
                            @else
                                <th class="{{ empty($param['mobile']) ? 'is-hidden-mobile' : '' }}">{!! ___('fields.' . $field) !!}</th>
                            @endif
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                        <tr class="{{ $item->is_new ? 'has-background-light has-text-weight-bold' : '' }}">
                            @if($actions['enebled'])
                                <td>
                                    <input type="checkbox" class="actions__checkbox" name="actions[]" value="{{ $item->id }}">
                                </td>
                            @endif
                            <td onclick="window.location='{{ route('admin.model.edit', [$slug, $item->id]) }}'" style="cursor: pointer;">{{ $item->id }}</td>
                            @foreach($fields as $field => $param)
                                <td class="{{ empty($param['mobile']) ? 'is-hidden-mobile' : '' }}" onclick="window.location='{{ route('admin.model.edit', [$slug, $item->id]) }}'" style="cursor: pointer;">
                                    @if($param['type'] === 'checkbox')
                                        <span class="icon {{ (isset($item->{$field}) && $item->{$field}) ? 'has-text-success' : 'has-text-danger' }}">
                                            <i class="fas {{ (isset($item->{$field}) && $item->{$field}) ? 'fa-check-square' : 'fa-ban' }}"></i>
                                        </span>
                                    @elseif($param['type'] === 'select')
                                        @php($additional = model($param['model'])::find($item->{$field}))
                                        {{ isset($additional) ? $additional->{$param['field']} : $item->{$field} }}
                                    @else
                                        {{ $item->{$field} }}
                                    @endif
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </form>
        @if($items->hasPages())
        <div class="section">
            <div class="container">
                {{ $items->appends(request()->input())->links('vault::pagination') }}
            </div>
        </div>
        @else
        <br>
        @endif

    @endif

@endsection
