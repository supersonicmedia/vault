<section class="hero {{ colour($slug) }} is-bold">
    <div class="hero-body">
        <div class="container">
            <h1 class="title">{{ config("vault.models.$slug.title", ucfirst($slug)) }}</h1>
            <h2 class="subtitle">
                <a href="{{ route('admin.model.index', [$slug]) }}">{{ __('vault::messages.back') }}</a>
            </h2>
        </div>
    </div>
</section>
@include('vault::resource.message')