@extends('vault::layout')

@section('content')

	@include('vault::resource.hero', [$slug])

	<div class="section" style="overflow: hidden;">
		<div class="container">

			<form action="{{ route('admin.model.update', [$slug, $item->id]) }}" method="post" enctype="multipart/form-data">
			<div class="columns">

				{{-- Main content --}}
				<div class="column is-three-quarters">
					{{ csrf_field() }}

					@if(photo($slug))
						<div class="field">
							<label class="label">{{ __('vault::fields.image') }}</label>
							<div class="file has-name is-fullwidth">
								<label class="file-label">
									<input class="file-input" type="file" name="add-image" id="file">
									<span class="file-cta">
										<span class="file-icon">
											<i class="fas fa-upload"></i>
										</span>
										<span class="file-label">{{ __('vault::messages.choose-file') }}</span>
									</span>
									<span class="file-name" id="filename"></span>
								</label>
							</div>
							<p class="help">{{ ___('messages.files_restriction') }}</p>
							@if($errors->has('add-image'))
							<p class="help is-danger">{{ $errors->first('add-image') }}</p>
							@endif
						</div>{{-- image --}}
					@endif

					@foreach($fields as $field => $param)
						@if($param['edit'])
							@include('vault::resource.fields.' . $param['type'], ['mode' => 'edit', 'field' => $field, 'param' => $param, 'errors' => $errors, 'item' => $item, 'slug' => $slug])
						@endif
					@endforeach

					<div class="field">
						<div class="control">
							<button type="submit" class="button is-link">
								<span class="icon"><i class="fas fa-save"></i></span>
	                            <span>{{ __('vault::messages.save') }}</span>
							</button>
						</div>
					</div>
				</div>
				{{-- Main content --}}

				{{-- Image and info --}}
				<div class="column">
					@if(photo($slug) && $item->medium_url)
						<div class="field">
						<a href="{{ $item->full_url }}" data-lity target="_blank">
							<figure class="image">
								<img src="{{ $item->thumb_url }}">
							</figure>
						</a>
						</div>
						<button onclick="return confirm('{{ __('vault::messages.confirm') }}')" type="submit" value="yes" name="delete-image" class="button is-danger">
							<span class="icon"><i class="fas fa-trash"></i></span>
                            <span>{{ __('vault::messages.delete-image') }}</span>
						</button>
						<hr>
					@endif
					<div class="field">
						<label for="" class="label">{{ __('vault::fields.created_at') }}</label>
						{{ $item->created_at }}
					</div>
					<div class="field">
						<label for="" class="label">{{ __('vault::fields.updated_at') }}</label>
						{{ $item->updated_at }}
					</div>

						{{--@if($item->files)
                        <hr>
                        <div class="buttons">
                            @foreach ($item->files as $file)
                            <a class="button is-fullwidth" href="{{ $file['url'] }}" download>
                                <span class="icon"><i class="fas fa-file"></i></span>
                                <span>{{ $file['name'] }}</span>
                            </a>
                            @endforeach
                        </div>
                        @endif--}}
				</div>
				{{-- Image and info --}}

			</div>
			</form>


		</div>{{-- container --}}
	</div>{{-- section --}}

	@include('vault::resource.gallery')
	@include('vault::resource.files')

	@if(deletable($slug))
	<section class="hero is-danger is-bold">
		<div class="hero-body">
			<div class="container">
				<div class="columns is-vcentered">
				<div class="column">
					<h1 class="title">Danger zone</h1>
				</div>
				<div class="column">
					<form class="" action="{{ route('admin.model.delete', [$slug, $item->id]) }}" method="post">
						{{ csrf_field() }}
						<button onclick="return confirm('{{ __('vault::messages.confirm') }}')" class="button is-danger is-inverted is-outlined">
							<span class="icon"><i class="fas fa-trash"></i></span>
                            <span>{{ __('vault::messages.delete') }}</span>
						</button>
					</form>
				</div>
				</div>
			</div>
		</div>
	</section>
	@endif

	<script type="text/javascript">
		var file = document.getElementById("file");
		file.onchange = function () {
			if (file.files.length > 0) {
				document.getElementById('filename').innerHTML = file.files[0].name;
			}
		};
	</script>

    @push('scripts')
        <script>
            $(document).ready(function () {
                $(document).bind('keydown', function(event) {
                    if (event.which == 27 && !($('input,textarea').is(':focus'))) {
                        window.location.href = '{{ route('admin.model.index', [$slug]) }}';
                        return false;
                    }
                });
            });
        </script>
    @endpush

@endsection
