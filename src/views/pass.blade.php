<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name') }}</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="{{ url('css/bulma.min.css') }}">
    <script defer src="{{ url('js/all.js') }}"></script>
    <style media="screen">
        body {
            overflow: hidden;
        }
        .hero {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .hero-body {
            justify-content: center;
            flex-direction: column;
            display: flex;
        }
    </style>
</head>
<body>
    <div class="hero is-light">
        <div class="hero-body">
            <div class="container">
                <div class="columns is-centered">
                    <div class="column is-half">
                        <form class="columns is-centered is-mobile" action="{{ route('pass') }}" method="post">
                            {{ csrf_field() }}
                            <div class="section">
                                <div class="field has-addons is-grouped-centered">
                                    <div class="control has-icons-left">
                                        <input class="input" type="password" name="pass" placeholder="Password" autofocus>
                                        <span class="icon is-small is-left"><i class="fas fa-lock"></i></span>
                                    </div>
                                    <div class="control">
                                        <button type="submit" class="button is-primary">Go</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>