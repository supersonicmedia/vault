@php($user = Auth::guard('admin')->user())
<nav class="navbar is-fixed-top has-shadow" role="navigation" aria-label="main navigation">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item" href="{{ route('admin.home') }}" title="{{ __('vault::messages.open-website') }}">
                @if(config('vault.logo'))
                <img src="{{ url(config('vault.logo')) }}" height="28">
                @else
                <b>{{ config('app.name') }}</b>
                @endif
            </a>

            <a href="#" class="navbar-burger burger">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">

                <a class="navbar-item" href="{{ route('admin.home') }}">
                    {{ __('vault::messages.dashboard') }}
                </a>

                <a class="navbar-item {!! Request::is("*settings*") ? 'is-active' : '' !!}" href="{{ route('admin.model.index', 'settings') }}">
                    {{ __('vault::messages.settings') }}
                </a>

                <a class="navbar-item {!! Request::is("*metrics*") ? 'is-active' : '' !!}" href="{{ route('admin.model.index', 'metrics') }}">
                    {{ __('vault::messages.metrics') }}
                </a>

                <a class="navbar-item" href="{{ url('/') }}" target="_blank">
                    {{ __('vault::messages.to_site') }}
                </a>

            </div>

            <div class="navbar-end">
                <div class="navbar-item" style="display:flex; align-items: center;">
                    <label class="tag is-info" style="margin-right: 10px;">{{ $user->name }}</label>
                    <a class="button is-light" href="{{ route('admin.logout') }}">
                        <span class="icon"><i class="fas fa-sign-out-alt"></i></span>
                        <span>{{ __('vault::messages.logout') }}</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>

@push('scripts')
    <script>
        $(document).ready(function() {

            // Check for click events on the navbar burger icon
            $(".navbar-burger").click(function() {

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                $(".navbar-burger").toggleClass("is-active");
                $(".navbar-menu").toggleClass("is-active");

            });
        });
    </script>
@endpush
