@if($paginator->hasPages())
    <nav class="pagination is-centered" role="navigation" aria-label="pagination">
        @if ($paginator->onFirstPage())
            <a class="pagination-previous" disabled title="{{ __('vault::messages.previous') }}">
                <i class="fas fa-arrow-left"></i>
            </a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="pagination-previous" title="{{ __('vault::messages.previous') }}">
                <i class="fas fa-arrow-left"></i>
            </a>
        @endif
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="pagination-next" title="{{ __('vault::messages.next') }}">
                <i class="fas fa-arrow-right"></i>
            </a>
        @else
            <a class="pagination-next" disabled title="{{ __('vault::messages.next') }}">
                <i class="fas fa-arrow-right"></i>
            </a>
        @endif
        <ul class="pagination-list">
            @foreach ($elements as $element)
                @if (is_string($element))
                    <li><span class="pagination-ellipsis">&hellip;</span></li>
                @endif
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        <li>
                            <a href="{{ $url }}" class="pagination-link {{ ($page == $paginator->currentPage()) ? 'is-current' : '' }}">{{ $page }}</a>
                        </li>
                    @endforeach
                @endif
            @endforeach
        </ul>
    </nav>
@endif