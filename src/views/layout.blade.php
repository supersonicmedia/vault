<!DOCTYPE html>
<html @auth('admin') class="has-navbar-fixed-top" @endauth>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }} | @yield('title', 'Admin panel')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="{{ url('css/bulma.min.css') }}">
    <script defer src="{{ url('js/all.js') }}"></script>
    @auth('admin')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.12.2/ui/trumbowyg.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pretty-checkbox/3.0.0/pretty-checkbox.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma-calendar@5.0.5/dist/css/bulma-calendar.min.css">
    <style media="screen">
        .textarea {min-height: 180px !important;transition: min-height .2s;}
        .textarea:focus {min-height: 300px !important;}
        .mce-content-body img {max-width: 100%; height: auto;}
        [data-lity] {cursor: zoom-in;}
        html, body {overflow-x: hidden;}
        .vault__notification {
            position: fixed;
            z-index: 10;
            min-width: 200px;
            max-width: 300px;
            right: 20px;
            bottom: 20px;
            opacity: .8;
            transition: all .2s;
            backface-visibility: visible !important;
            animation-name: flipInX;
            animation-duration: .5s;
        }
        .vault__notification:hover {opacity: 1;}
        @media (max-width: 768px) {
            .vault__notification {left: 20px; min-width: none; max-width: none;}
        }
        @keyframes flipInX {
            from {
                transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
                animation-timing-function: ease-in;
                opacity: 0;
            }

            40% {
                transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
                animation-timing-function: ease-in;
            }

            60% {
                transform: perspective(400px) rotate3d(1, 0, 0, 10deg);
                opacity: 1;
            }

            80% {
                transform: perspective(400px) rotate3d(1, 0, 0, -5deg);
            }

            to {
                transform: perspective(400px);
            }
        }
    </style>
    @endauth
    @stack('meta')
</head>
<body>

    @auth('admin')
        @include('vault::navbar')
    @endauth

    @yield('content')


    @auth('admin')

    @if(request()->url() !== route('admin.home'))
    @include('vault::footer')
    @endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bulma-calendar@5.0.4/dist/js/bulma-calendar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
    @push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var editor_config = {
                path_absolute : "/",
                selector: "textarea.wysiwyg",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });
                }
            };

            tinymce.init(editor_config);

            $('.add-hour, .sub-hour').on('click', function (e) {
                e.preventDefault();

                var input = $(this).parents('.columns').first().find('input');
                var new_date = moment(input.val());

                if ($(this).hasClass('add-hour')) {
                    new_date.add('hours', 1);
                } else {
                    new_date.subtract('hours', 1);
                }

                input.val(new_date.format('YYYY-MM-DD HH:mm:ss'));

            });

            var calendars = bulmaCalendar.attach('[type=date]', {
                dateFormat: 'YYYY-MM-DD HH:mm:ss',
                showHeader: false,
                lang: '{{ app()->getLocale() }}',
                showFooter: false,
                displayMode: 'dialog',
                weekStart: 1,
            });

            if ($('[data-start-date]').length) {
                $('[data-lity]').on('click', function (e) {
                    e.preventDefault();
                    lity($(this).attr('href'));
                });
            }

        });
    </script>
    @endpush
    @endauth

    @stack('scripts')

</body>
</html>
