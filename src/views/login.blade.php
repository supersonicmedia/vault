@extends('vault::layout')

@section('content')
    <div class="hero is-light is-fullheight">
        <div class="hero-body">
            <div class="container">
                <div class="columns is-centered">
                    <div class="column is-half">
                        <h1 class="subtitle">{{ __('vault::messages.login') }}</h1>
                        <form class="" action="{{ route('admin.login') }}" method="post">
                            {{ csrf_field() }}
                            <div class="field">
                                <input class="input" type="text" name="email" value="" placeholder="E-mail">
                            </div>
                            <div class="field">
                                <input type="password" name="password" value="" class="input" placeholder="{{ __('vault::messages.password') }}">
                            </div>
                            <div class="field">
                                <button type="submit" name="button" class="button is-primary">
                                    <span class="icon"><i class="fas fa-sign-in-alt"></i></span>
                                    <span>{{ __('vault::messages.login') }}</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection