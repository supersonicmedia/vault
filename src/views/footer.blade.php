<footer class="footer">
    <div class="content has-text-centered">
        <p>
            <strong>The Vault</strong> by <a target="_blank" href="https://rockingtech.co.uk/?utm_source=vault&utm_campaign={{ strtolower(config('app.name')) }}">Rocking Tech</a>. {{ date('Y') }}
        </p>
    </div>
</footer>
