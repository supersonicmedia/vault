@extends('vault::layout')

@section('content')

    <section class="hero is-primary is-fullheight-with-navbar is-bold">
        <div class="hero-body">
            <div class="container">
                <form action="{{ route('admin.help') }}" method="post" id="help__form" autocomplete="off">
                    @csrf
                    <input autocomplete="off" type="text" name="help" value="{{ isset($search_request) ? $search_request : '' }}" class="is-size-1-desktop is-size-3-tablet is-size-5-mobile has-text-white" id="help__input" placeholder="{{ __('vault::messages.hi') }}, {{ auth('admin')->user()->name }}. {{ __('vault::messages.just_typing') }}" autofocus>
                </form>
                <br>
                <div id="help__result">
                    @if(isset($search_result) && count($search_result) > 0)
                        @foreach($search_result as $tip)
                            <article class="message is-primary">
                                <div class="message-body">{{ $tip }}</div>
                            </article>
                        @endforeach
                    @endif
                </div>
                <br>
                @foreach($models_groups as $group)
                    @if($models_have_group)
                        <h3 class="subtitle">{{ $group ?? __('vault::messages.group') }}</h3>
                    @endif
                    <div class="columns is-mobile is-multiline animated fadeInUp" @if($loop->index === 0) id="main-menu" @endif>
                        @foreach($models->where('group', $group) as $slug => $model)
                            <div class="column is-half-mobile is-one-fifth-desktop is-one-quarter-tablet is-2-widescreen is-full-xs">
                                <a class="button is-primary is-inverted is-outlined is-fullwidth is-block is-clipped" style="white-space: nowrap; text-overflow: ellipsis;" href="{{ isset($model['url']) ? $model['url'] : (isset($model['route']) ? route($model['route']) : route('admin.model.index', $slug)) }}" @if(isset($model['is_blank']) && $model['is_blank']) target="_blank" @endif>
                                    {{-- <span class="icon"><i class="fab fa-github"></i></span> --}}
                                    <span>
                                        {{ $model['title'] }}
                                        @if(isset($model['name']) && unread($model['name']))
                                            &nbsp;<b>({{ unread($model['name']) }})</b>
                                        @endif
                                    </span>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach

            </div>
        </div>
    </section>

@endsection


@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js"></script>
    <script>
        $(document).ready(function ($) {
            var helpForm   = $('#help__form');
            var helpInput  = $('#help__input');
            var helpResult = $('#help__result');

            helpForm.submit(function (e) {
                e.preventDefault();
            });

            helpInput.bind("keyup change", $.debounce(250, function(e) {
                e.preventDefault();

                $("#main-menu .column").each(function () {
                    var found = $("a span", this).text().toLowerCase().match(helpInput.val().toLowerCase())
                    found ? $(this).show() : $(this).hide();
                });

                if($("#main-menu .column:visible").length == 0) {
                    $("#main-menu .column").show();
                }

                var queryLength = helpInput.val().length;
                if (queryLength == 0) {
                    helpResult.html('');
                    $("#main-menu .column").show();
                } else if (queryLength >= 3) {
                    $.ajax({
                        type: helpForm.attr('method'),
                        url:  helpForm.attr('action'),
                        data: helpForm.serialize(),
                    }).done(function(response) {

                        if (response.status === 1) {
                            var searchResult = response.search_result;
                            if (searchResult.length > 0) {
                                var rendered = "";
                                for(var i in searchResult) {
                                    var temp = '<article class="message is-primary animated fadeInUp"><div class="message-body">' + searchResult[i] + "</div></article>";
                                    rendered += temp;
                                }
                                helpResult.html(rendered);
                            }
                        } else {
                            console.log('Error');
                        }
                    });
                }

            }));
        });
    </script>
@endpush

@push('meta')
    <style media="screen">
        #help__input {background: none; outline: none; border: none; display: block; width: 100%;}
        #help__input::-webkit-input-placeholder {color: rgba(255, 255, 255, .5);}
        #help__input::-moz-placeholder {color: rgba(255, 255, 255, .5);}
        #help__input:-ms-input-placeholder {color: rgba(255, 255, 255, .5);}
        #help__input:-moz-placeholder {color: rgba(255, 255, 255, .5);}

        .animated {
            -webkit-animation-duration: .4s;
            -o-animation-duration: .4s;
            -moz-animation-duration: .4s;
            animation-duration: .4s;
        }

        @-webkit-keyframes fadeInUp {
            from {
                opacity: 0;
                -webkit-transform: translate3d(0, 100%, 0);
                transform: translate3d(0, 100%, 0);
            }

            to {
                opacity: 1;
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
            }
        }

        @keyframes fadeInUp {
            from {
                opacity: 0;
                -webkit-transform: translate3d(0, 100%, 0);
                transform: translate3d(0, 100%, 0);
            }

            to {
                opacity: 1;
                -webkit-transform: translate3d(0, 0, 0);
                transform: translate3d(0, 0, 0);
            }
        }

        .fadeInUp {
            -webkit-animation-name: fadeInUp;
            animation-name: fadeInUp;
        }
    </style>
@endpush
