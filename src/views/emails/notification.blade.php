@extends('vault::emails.layout')

@section('content')
    <div class="section">
        <div class="columns is-centered">
            <div class="column">
                <h1 class="title">{{ __('vault::messages.notification') }}</h1>
                <div class="content">
                    <p>{{ $text }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection