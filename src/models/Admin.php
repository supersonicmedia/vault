<?php

namespace Supersonic\Vault\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\User;
use Log;
use Auth;

class Admin extends User {

    protected $table = 'users';

    /**
     * The "booting" method of the model.
     * @return void
     */
    protected static function boot () {
        parent::boot();

        static::addGlobalScope('is_admin', function (Builder $builder) {
            $builder->where('is_admin', true);
        });
    }

    public function log ($slug, $action, $item = null) {
        
        $actions = [
            'store'  => __('vault::messages.created'),
            'read'   => __('vault::messages.opened'),
            'update' => __('vault::messages.updated'),
            'delete' => __('vault::messages.deleted'),
        ];

        $id = $item ? ", ID #{$item->id}" : '';
        
        $str = [
            __('vault::messages.user'),
            " #{$this->id} ({$this->name}, {$this->email}) ",
            mb_strtolower($actions[$action]),
            ' "',
            config("vault.models.$slug.title", ucfirst($slug)),
            '"',
            $id
        ];
        
        $str = implode($str);
        Log::channel('vault')->info($str);
        
    }

}