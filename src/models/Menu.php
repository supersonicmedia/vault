<?php

namespace Supersonic\Vault\Models;

use Supersonic\Vault\Models\BaseModel as BaseModel;

class Menu extends BaseModel {

    protected $table = 'menu';

    protected $fillable = [
        'title',
        'link',
        'parent_id',
        'is_blank',
        'sorting_order',
    ];

    /**
     * Formatting link url
     * @return [type] $link [Formatted link]
     */
    public function getLinkFormattedAttribute () {
        $link = $this->link;

        if (!filter_var($link, FILTER_VALIDATE_URL)) {
            $link = url($link);
        }

        return $link;
    }

    public function getBlankAttribute () {
        return $this->is_blank ? 'target=_blank' : '';
    }

    public function scopeParents ($query) {
        return $query->where('parent_id', 0)->orWhere('parent_id', null);
    }

    public function parent()
    {
        return $this->belongsTo('Supersonic\Vault\Models\Menu', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('Supersonic\Vault\Models\Menu', 'parent_id');
    }

}
