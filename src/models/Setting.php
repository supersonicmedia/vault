<?php

namespace Supersonic\Vault\Models;

use Illuminate\Database\Eloquent\Builder;
use Supersonic\Vault\Models\BaseModel as BaseModel;
use App;

class Setting extends BaseModel {

    protected $fillable = [
        'property',
        'value',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('property', function (Builder $builder) {
            $builder->where('property', 'not like', 'tracking_%');
        });
    }

    /**
     * Returns ready-to-use row
     * @param  [type] $property [description]
     * @return [type]                [description]
     */
    public static function show ($property) {
        $show = self::where('property', $property)->first();
        return $show ? $show->value : '';
    }

    /**
     * Setting name attribute. Appears on the Settings index page as a read-only attribute.
     * @return [type] [description]
     */
    public function getPropertyAttribute () {
        return ___('fields.' . $this->getOriginal('property'));
    }

}
