<?php

namespace Supersonic\Vault\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class BaseModel extends Model implements HasMedia {

    use HasMediaTrait;

    public function registerMediaConversions (Media $media = null) {
        $this->addMediaConversion('thumb')->width(300)->height(300)->nonQueued();
        $this->addMediaConversion('medium')->width(600)->height(600)->nonQueued();
        $this->addMediaConversion('large')->width(1000)->height(1000)->nonQueued();
        $this->addMediaConversion('full')->width(1600)->height(1600)->nonQueued();
        $this->addMediaConversion('thumb-webp')->format('webp')->width(300)->height(300)->nonQueued();
        $this->addMediaConversion('medium-webp')->format('webp')->width(600)->height(600)->nonQueued();
        $this->addMediaConversion('large-webp')->format('webp')->width(1000)->height(1000)->nonQueued();
        $this->addMediaConversion('full-webp')->format('webp')->width(1600)->height(1600)->nonQueued();
        $this->addMediaConversion('source-webp')->format('webp')->nonQueued();
    }

    public function getMediaUrl ($size, $collection = 'main') {
        if ($collection) {
            $pics = $this->getMedia($collection);
        } else {
            $pics = $this->getMedia();
        }

        if(!$pics) return '';

        $pic = $pics->first();
        if($pic) return url(trim($pic->getUrl($size), '/'));

        return '';
    }


    public function scopeRecent ($query) {
        return $query->orderBy('created_at', 'DESC');
    }

    public function scopeActive ($query) {
        return $query->where('is_active', true);
    }

    public function scopeNew ($query) {
        return $query->where('is_new', true);
    }

    public function getSmallUrlAttribute () {return $this->getMediaUrl('thumb');}
    public function getThumbUrlAttribute () {return $this->getMediaUrl('thumb');}
    public function getMediumUrlAttribute () {return $this->getMediaUrl('medium');}
    public function getLargeUrlAttribute () {return $this->getMediaUrl('large');}
    public function getBigUrlAttribute () {return $this->getMediaUrl('large');}
    public function getFullUrlAttribute () {return $this->getMediaUrl('full');}
    public function getSourceUrlAttribute () {return $this->getMediaUrl('');}
    public function getSmallWebpUrlAttribute () {return $this->getMediaUrl('thumb-webp');}
    public function getThumbWebpUrlAttribute () {return $this->getMediaUrl('thumb-webp');}
    public function getMediumWebpUrlAttribute () {return $this->getMediaUrl('medium-webp');}
    public function getLargeWebpUrlAttribute () {return $this->getMediaUrl('large-webp');}
    public function getBigWebpUrlAttribute () {return $this->getMediaUrl('large-webp');}
    public function getFullWebpUrlAttribute () {return $this->getMediaUrl('full-webp');}
    public function getSourceWebpUrlAttribute () {return $this->getMediaUrl('source-webp');}

    /**
     * Photos from gallery
     * @return array [description]
     */
    public function getGalleryAttribute ():array
    {
        $pics = $this->getMedia('gallery');
        if(!$pics) return null;

        $array = [];

        foreach ($pics as $pic) {
            $array[] = [
                'id'          => $pic->id,
                'thumb'       => url(trim($pic->getUrl('thumb'), '/')),
                'medium'      => url(trim($pic->getUrl('medium'), '/')),
                'large'       => url(trim($pic->getUrl('large'), '/')),
                'full'        => url(trim($pic->getUrl('full'), '/')),
                'thumb_webp'  => url(trim($pic->getUrl('thumb-webp'), '/')),
                'medium_webp' => url(trim($pic->getUrl('medium-webp'), '/')),
                'large_webp'  => url(trim($pic->getUrl('large-webp'), '/')),
                'full_webp'   => url(trim($pic->getUrl('full-webp'), '/')),
                'orientation' => $pic->hasCustomProperty('orientation') ? $pic->getCustomProperty('orientation') : null,
            ];
        }

        return $array;
    }

    /**
     * Get the files attached.
     * @return array [Links]
     */
    public function getFilesAttribute ():array
    {
        $files = $this->getMedia('files');

        if(!$files) {
            return [];
        }

        $array = [];

        foreach ($files as $file) {
            $array[] = [
                'id'        => $file->id,
                'name'      => $file->name,
                'url'       => url(trim($file->getUrl(), '/')),
                'mime_type' => $file->mime_type
            ];
        }

        return $array;
    }

    /**
     * Returns number of new (unread) items
     * @return int [description]
     */
    public static function countNew (): int
    {
        return (int) self::new()->count();
    }

}
