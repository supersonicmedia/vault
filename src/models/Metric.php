<?php

namespace Supersonic\Vault\Models;

use Illuminate\Database\Eloquent\Builder;
use Supersonic\Vault\Models\Setting;
use App;

class Metric extends Setting {

    protected $table = 'settings';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('property', function (Builder $builder) {
            $builder->where('property', 'like', 'tracking_%');
        });
    }

}
