<?php

namespace Supersonic\Vault\Controllers;

use Facade\Ignition\Support\Packagist\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;
use Auth;
use Supersonic\Vault\Models\Admin;
use Carbon\Carbon;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Supersonic\Vault\Exports\ModelExport;
use Supersonic\Vault\Imports\ModelImport;
use Validator;

class ResourceController extends Controller {

    public function __construct ()
    {
        $this->middleware('admin');
    }

    /**
     * Returns model outline from the config
     * @param  [type] $slug [description]
     * @return array        [description]
     */
    public static function getEntity ($slug): array
    {

        $entity = entity($slug);

        if (!$entity) {
            abort(404);
        }

        return $entity;

    }

    /**
     * Returns a set of fields, availiable for showing
     * @param  [type] $slug [model slug, e.g. 'users']
     * @return [type]       [description]
     */
    public static function getFields ($slug, $mode = 'edit'): array
    {

        $entity = config('vault.models.' . $slug);
        return array_filter($entity['fields'], function ($val) use ($mode) {
            return (bool) $val[$mode];
        });

    }

    /**
     * Returns rules of validation for a field
     * @param  string $slug [description]
     * @param  string $mode [description]
     * @param  [type] $id   [description]
     * @return array        Rules
     */
    public static function getRules (string $slug, string $mode = 'store', int $id = null): array
    {
        $entity = config('vault.models.' . $slug);

        // Filter fields. We are interested in those, which contains 'validate' rules
        $filtered = array_filter($entity['fields'], function ($val) {
            return isset($val['validate']) || isset($val['validate_store']);
        });

        // Overwrite validation rules if there are special validate_store rules
        if ($mode = 'store') {
            foreach ($filtered as $key => &$value) {
                if (isset($value['validate_store'])) {
                    $value['validate'] = $value['validate_store'];
                }
            }
        }

        // Extract just one column from filtered values
        $values = array_column($filtered, 'validate');

        // Unique fields
        if ($mode = 'update' && isset($id)) {
            foreach ($values as $key => $value) {
                // Add id behind unique rules
                if (preg_match("/unique\:\w{0,},\w{0,}/", $value, $matches)) {
                    $values[$key] = preg_replace("/unique\:\w{0,},\w{0,}/", $matches[0] . ',' . $id, $value);
                }
            }
        }

        // Combine validation rules and keys
        $rules = array_combine(array_keys($filtered), $values);

        // Add rules for images
        $rules['add-image'] = 'sometimes|image|dimensions:min_width=100,min_height=100|max:5000';

        return $rules;

    }

    /**
     * List all items
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function index (Request $request, string $slug)
    {

        $entity = self::getEntity($slug);

        // If there is no such entity
        if (is_null($entity)) {
            abort(404);
        }

        $classname = model($entity['name']);

        // Selecting only those fields, which are allowed to be shown
        $fields = self::getFields($slug, 'show');

        // Default orderBy fields
        $field = 'id';
        $order = 'DESC';

        // Ordering
        if (isset($request->sort)) {
            $sort = explode('-', $request->sort);
            if (count($sort) == 2 && in_array($sort[0], array_keys($fields)) && in_array($sort[1], ['asc', 'desc'])) {
                $field = $sort[0];
                $order = $sort[1];
            } else {
                return redirect()->route('admin.model.index', $slug);
            }
        }

        $items = $classname::orderBy($field, $order);

        // Filter
        if (isset($request->filter)) {
            foreach ($request->filter as $filter_name => $filter_value) {
                if ($filter_value !== 'clear_filter') $items = $items->where($filter_name, $filter_value);
            }
        }

        $items = $items->paginate(10);

        // Actions
        $actions = [
            'enebled' => false,
            'delete' => false,
            'activity' => false,
        ];

        if (deletable($slug)) {
            $actions['enebled'] = true;
            $actions['delete'] = true;
        }
        if (isset($entity['fields']['is_active'])) {
            $actions['enebled'] = true;
            $actions['activity'] = true;
        }

        return view('vault::resource.index', compact('items', 'slug', 'fields', 'actions'));

    }

    /**
     * Show create form
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function create ($slug)
    {

        if (!creatable($slug)) {
            return redirect()->route('admin.model.index', [$slug]);
        }

        $fields = self::getFields($slug, 'create');
        return view('vault::resource.create', compact( 'slug', 'fields'));

    }

    /**
     * Show edit form
     * @param  [type] $slug [description]
     * @param  [type] $id   [description]
     * @return [type]       [description]
     */
    public function edit (string $slug, int $id)
    {

        $entity = self::getEntity($slug);
        $classname = model($entity['name']);
        $item = $classname::findOrFail($id);

        if (!is_null($item->is_new)) {
            $item->update(['is_new' => false]);
        }

        // Selecting fields, which are allowed to be edited (be default in getFields)
        $fields = self::getFields($slug);

        // Log
        auth('admin')->user()->log($slug, 'read', $item);

        return view('vault::resource.edit', compact('item', 'slug', 'fields'));

    }

    public function store ($slug, Request $request) {

        $entity = self::getEntity($slug);
        $classname = model($entity['name']);

        // Message for validation errors
        session()->flash('message', __('vault::messages.validate_error'));
        session()->flash('type', 'danger');

        // Validation
        $this->validate($request, self::getRules($slug));

        $item = new $classname($this->values($entity['fields'], $request, 'create'));
        $item->save();

        // Log
        auth('admin')->user()->log($slug, 'store', $item);

        return redirect()->route('admin.model.edit', [$slug, $item->id])->with(['message' => trans('vault::messages.success'), 'type' => 'success']);

    }

    public function update (Request $request, $slug, $id)
    {

        $entity = self::getEntity($slug);
        $classname = model($entity['name']);
        $item = $classname::findOrFail($id);

        // Message for validation errors
        session()->flash('message', __('vault::messages.validate_error'));
        session()->flash('type', 'danger');

        // Delete a photo from the gallery
        if($request->{'delete-gallery'}) {
            try {
                $media = $item->getMedia('gallery')->where('id', $request->{'delete-gallery'})->first();
                if ($media) {
                    $media->delete();
                }
            } catch (\Exception $e) {
                Log::info($e);
                return redirect()->route('admin.model.edit', [$slug, $item->id])->with(['message' => __('vault::messages.error'), 'type' => 'danger']);
            }
            return redirect()->route('admin.model.edit', [$slug, $item->id, '#gallery'])->with(['message' => __('vault::messages.success'), 'type' => 'success']);
        }

        if ($request->orientation) {
            foreach ($request->orientation as $id => $orientation) {
                if (is_numeric($id) && in_array($orientation, ['horizontal', 'vertical'])) {
                    $mediaItem = $item->getMedia('gallery')->where('id', $id)->first();
                    if ($mediaItem) {
                        $mediaItem->setCustomProperty('orientation', $orientation);
                        $mediaItem->save();
                    }
                }
            }
            return redirect()->route('admin.model.edit', [$slug, $item->id, '#gallery'])->with(['message' => __('vault::messages.success'), 'type' => 'success']);
        }

        // Add a photo to the gallery
        if($request->file('gallery')) {

            $count_succes = 0;
            $count_errors = 0;

            $input = $request->all();

            $validator = Validator::make(
                $input,
                [
                    'gallery.*' => 'sometimes|image|dimensions:min_width=100,min_height=100|max:5000'
                ],[
                    'gallery.*.dimensions' => 'Wrong width or height!',
                    'gallery.*.image' => 'Only images are allowed',
                    'gallery.*.max' => 'Sorry! Maximum allowed size for an image is 5MB',
                ]
            );

            foreach ($request->file('gallery') as $index => $file) {

                if (!$validator->errors()->has('gallery.' . $index)) {
                    try {
                        $item->addMedia($file)->toMediaCollection('gallery');
                        $count_succes+=1;
                    } catch (\Exception $e) {
                        Log::info($e);
                        $count_errors+=1;
                    }
                } else {
                    $count_errors+=1;
                }
            }

            if ($count_errors) {
                return redirect()->route('admin.model.edit', [$slug, $item->id])->with(['message' => __('vault::messages.warning_files', ['skip' => $count_errors]), 'type' => 'warning']);
            }

            return redirect()->route('admin.model.edit', [$slug, $item->id, '#gallery'])->with(['message' => __('vault::messages.success'), 'type' => 'success']);
        }

        // Delete a file from the files
        if($request->{'delete-files'}) {
            try {
                $media = $item->getMedia('files')->where('id', $request->{'delete-files'})->first();
                if ($media) {
                    $media->delete();
                }
            } catch (\Exception $e) {
                Log::info($e);
                return redirect()->route('admin.model.edit', [$slug, $item->id])->with(['message' => __('vault::messages.error'), 'type' => 'danger']);
            }
            return redirect()->route('admin.model.edit', [$slug, $item->id, '#files'])->with(['message' => __('vault::messages.success'), 'type' => 'success']);
        }

        // Add a file to the files
        if($request->file('files')) {

            $count_succes = 0;
            $count_errors = 0;

            $input = $request->all();

            $validator = Validator::make(
                $input,
                [
                    'files.*' => 'sometimes|max:10000'
                ],[
                    'files.*.max' => 'Sorry! Maximum allowed size for an image is 10MB',
                ]
            );

            foreach ($request->file('files') as $index => $file) {

                if (!$validator->errors()->has('files.' . $index)) {
                    try {
                        $item->addMedia($file)->toMediaCollection('files');
                        $count_succes+=1;
                    } catch (\Exception $e) {
                        Log::info($e);
                        $count_errors+=1;
                    }
                } else {
                    $count_errors+=1;
                }
            }

            if ($count_errors) {
                return redirect()->route('admin.model.edit', [$slug, $item->id])->with(['message' => __('vault::messages.warning_files', ['skip' => $count_errors]), 'type' => 'warning']);
            }

            return redirect()->route('admin.model.edit', [$slug, $item->id, '#files'])->with(['message' => __('vault::messages.success'), 'type' => 'success']);
        }

        // Validation
        $this->validate($request, self::getRules($slug, 'update', $id));

        $item->update($this->values($entity['fields'], $request, 'edit'));

        if($request->file('add-image')) {
            try {
                $item->clearMediaCollection('main');
                $item->addMedia($request->file('add-image'))->toMediaCollection('main');
            } catch (\Exception $e) {
                Log::info($e);
                return redirect()->route('admin.model.edit', [$slug, $item->id])->with(['message' => __('vault::messages.error'), 'type' => 'danger']);
            }
        }

        if($request->{'delete-image'} === 'yes') {
            try {
                $item->clearMediaCollection('main');
            } catch (\Exception $e) {
                Log::info($e);
                return redirect()->route('admin.model.edit', [$slug, $item->id])->with(['message' => __('vault::messages.error'), 'type' => 'danger']);
            }
        }

        // Log
        auth('admin')->user()->log($slug, 'update', $item);

        return redirect()->route('admin.model.edit', [$slug, $item->id])->with(['message' => __('vault::messages.success'), 'type' => 'success']);

    }

    public function destroy ($slug, $id) {

        $entity = self::getEntity($slug);

        if (!deletable($slug)) {
            return redirect()->route('admin.model.edit', [$slug, $id]);
        }

        $classname = model($entity['name']);
        $item = $classname::findOrFail($id);

        $item->delete();

        // Log
        auth('admin')->user()->log($slug, 'delete', $item);

        return redirect()->route('admin.model.index', [$slug])->with(['message' => __('vault::messages.success'), 'type' => 'success']);
    }

    /**
     * List of current values of an item
     * @param  [type] $fields  [description]
     * @param  [type] $request [description]
     * @param  [type] $type    [description]
     * @return array           [description]
     */
    public function values ($fields, $request, $type): array
    {

        foreach ($fields as $field => $param) {
            if ($param[$type]) {
                switch ($param['type']) {
                    case 'checkbox':
                        $values[$field] = $request->{$field} === 'on';
                        break;
                    case 'password':
                        $values[$field] = bcrypt($request->{$field});
                        break;
                    case 'date':
                        $values[$field] = Carbon::parse($request->{$field});
                        break;
                    case 'json':
                    // dd($request->{$field});
                        $result = array_combine($request->{$field}['label'], $request->{$field}['value']);
                        $result = array_filter($result);
                        $result = json_encode($result);
                        $values[$field] = $result;
                        break;
                    default:
                        $values[$field] = $request->{$field};
                        break;
                }
            }
        }

        return $values;

    }

    /**
     * Export records to xls
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function export ($slug)
    {
        $pieces = [
            strtolower(config('app.name')),
            "__{$slug}_",
            date('d.m.Y'),
            '.xlsx'
        ];
        $filename = implode($pieces);
        return Excel::download(
            new ModelExport($slug),
            $filename
        );
    }

    /**
     * Import records from xls to database
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function import ($slug, Request $request)
    {
        // Message for validation errors
        session()->flash('message', __('vault::messages.error'));
        session()->flash('type', 'danger');

        $request->validate([
            'import_file' => 'required'
        ]);

        $validation_rules = self::getRules($slug);

        try {
            $import = new ModelImport($slug, $validation_rules);
            $import->import($request->file('import_file'));
        } catch (\Exception $e) {
            Log::info($e);
            return redirect()->route('admin.model.index', [$slug])->with(['message' => __('vault::messages.error'), 'type' => 'danger']);
        }

        // Count skipped rows (errors and validator)
        if ($import->errors() || $import->failures()) {
            $count_skip = count($import->errors()) + count($import->failures());
            return redirect()->route('admin.model.index', [$slug])->with(['message' => __('vault::messages.warning_import', ['skip' => $count_skip]), 'type' => 'warning']);
        }

        return redirect()->route('admin.model.index', [$slug])->with(['message' => __('vault::messages.success'), 'type' => 'success']);
    }

    public function actions (Request $request, $slug)
    {
        if (!$request->actions) return redirect()->route('admin.model.index', [$slug])->with(['message' => __('vault::messages.actions'), 'type' => 'warning']);

        $entity = self::getEntity($slug);
        $classname = model($entity['name']);
        $items = $classname::whereIn('id', $request->actions)->get();

        if ($request->type == 'delete') {
            if (!deletable($slug)) {
                return redirect()->route('admin.model.index', [$slug])->with(['message' => __('vault::messages.error'), 'type' => 'error']);
            }

            foreach ($items as $item) {
                $item->delete();

                // Log
                auth('admin')->user()->log($slug, 'delete', $item);
            }
        }

        if ($request->type == 'activate' || $request->type == 'deactivate') {
            foreach ($items as $item) {
                $item->update([
                    'is_active' => $request->type == 'activate',
                ]);

                // Log
                auth('admin')->user()->log($slug, 'update', $item);
            }

        }

        return redirect()->route('admin.model.index', [$slug]);
    }

}
