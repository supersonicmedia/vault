<?php

namespace Supersonic\Vault\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class MainController extends Controller {

    public function __construct () {
        $this->middleware('admin', ['except' => ['pass']]);
    }

    /**
     * Dashboard
     * @return [type] [description]
     */
    public function index () {

        // Model names
        $models = config('vault.models');
        $buttons = config('vault.buttons');

        if (is_array($models) && is_array($buttons)) $models = array_merge($models, $buttons);

        $models = collect($models)->sortBy('group')->sortBy('title')->forget(['settings', 'metrics']);
        $models_have_group = $models->where('group')->count();
        $models_groups = $models->pluck('group')->unique();

        return view('vault::index', compact('models', 'models_have_group', 'models_groups'));

    }

    /**
     * Help page
     * This function can accept AJAX requests as well.
     * @return [type] [description]
     */
    public function help (Request $request) {

        $ajax           = $request->ajax();
        $search_request = $request->help;

        if ($ajax || $request->isMethod('post')) {
            $search_result = [];

            if (file_exists(base_path('readme.md'))) {
                $readme_file   = file_get_contents(base_path('readme.md'));
                $readme_file   = preg_replace( '/\r|\n/', '', $readme_file);
                $readme        = explode('*', $readme_file);

                foreach ($readme as $tip) {
                    if (mb_stripos($tip, $search_request, 0, 'UTF-8')) {
                        $search_result[] = $tip;
                    }
                }

                if (count($search_result) === 0) {
                    foreach ($readme as $tip) {
                        if (mb_stripos($tip, switcher($search_request,2), 0, 'UTF-8')) {
                            $search_result[] = $tip;
                        }
                    }
                }

            }

            if ($ajax) {
                return response()->json([
                    'status'        => 1,
                    'search_result' => $search_result,
                ]);
            } else {
                return view('vault::index', compact('search_result', 'search_request'));
            }
        } else {
            return view('vault::index');
        }

    }

    /**
     * Access
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function pass (Request $request) {

        if (session('pass')) {
            return redirect('/');
        }

        if ($request->isMethod('get')) {

            session()->put('from', url()->previous());
            return view('vault::pass');

        } else if ($request->isMethod('post')) {

            if ($request->pass == config('vault.password')) {

                session(['pass' => true]);

                if (session()->has('from')) {

                    $from = session('from');
                    session()->forget('from');

                    return redirect($from);

                } else {
                    // Not using route, because it could be different from 'index' or 'home'
                    return redirect('/');
                }


            } else {
                return redirect()->route('pass');
            }

        }
    }

}
