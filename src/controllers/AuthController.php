<?php

namespace Supersonic\Vault\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AuthController extends Controller {
    
    public function __construct () {
        $this->middleware('login', ['except' => ['logout']]);
    }
    
    public function auth (Request $request) {

        $credentials = $request->only('email', 'password');
        
        if (Auth::guard('admin')->attempt($credentials)) {

            // Authentication passed
            return redirect()->route('admin.home');
            
        }
        
        return redirect()->route('admin.login');
        
    }
    
    public function login () {
        
        return view('vault::login');
        
    }
    
    public function logout () {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
        
    }
    
}