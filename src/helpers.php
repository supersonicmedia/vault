<?php

    /**
     * Displaying settings
     * @param  [type] $property [description]
     * @return [type]           [description]
     */
    function setting ($property): string
    {
        return Supersonic\Vault\Models\Setting::show($property);
    }

    /**
     * Pick a colour based on the length of the slug
     * @param  [type] $string [description]
     * @return [type]         [description]
     */
    function colour ($string): string
    {

        $colours = [
            'Link',
            'Info',
            'Success',
            'Warning',
            'Light',
            'Dark',
        ];

        $length = strlen($string);
        $num = $length % count($colours);

        return 'is-' . strtolower($colours[$num]);

    }

    /**
     * Alternative translation helper
     * @param  [type] $string [description]
     * @return [type]         [description]
     */
    function ___ ($string): string
    {
        // Check if we have value in project
        $has_lang = Lang::has("vault.$string");
        if ($has_lang) return __("vault.$string");

        // If we don't have, then check in vault
        $has_lang = Lang::has("vault::$string");
        if ($has_lang) return __("vault::$string");

        // So, if we don't have translation then get last partion of string and formatting
        $string = explode('.', $string);
        $string = end($string);

        return ucfirst(str_replace('_', ' ', $string));

    }

    /**
     * Returns full path to a model based on the classname
     * @param  string $name [description]
     * @return string       [description]
     */
    function model (string $name): string
    {

        switch ($name) {
            case 'User':
                $classname = "App\\" . $name;
                break;
            case 'Setting':
                $classname = "Supersonic\\Vault\\Models\\" . $name;
                break;
            case 'Metric':
                $classname = "Supersonic\\Vault\\Models\\" . $name;
                break;
            case 'Menu':
                $classname = "Supersonic\\Vault\\Models\\" . $name;
                break;
            default:
                $classname = "App\\Models\\" . $name;
                break;
        }

        return $classname;

    }

    /**
     * Returns a number of unread (new) items
     * @param  string $classname [description]
     * @return int               [description]
     */
    function unread (string $classname): int
    {

        $class = model($classname);
        $tablename = (new $class())->getTable();

        if (Schema::hasColumn($tablename, 'is_new')) {
            return $class::where('is_new', 1)->count();
        } else {
            return 0;
        }

    }

    /**
     * Returns model outline from the config
     * @param  [type] $slug [description]
     * @return array        [description]
     */
    function entity ($slug): array
    {

        $entity = config('vault.models.' . $slug);

        if ($entity && is_array($entity)) {
            return $entity;
        }

        return [];


    }

    /**
     * Checks if a record can be deleted
     * @param  [type] $slug [description]
     * @return bool         [description]
     */
    function deletable ($slug): bool
    {

        $entity = entity($slug);

        return isset($entity['delete']) && $entity['delete'] == 1;

    }

    /**
     * Checks if a record can add gallery
     * @param  [type] $slug [description]
     * @return bool         [description]
     */
    function gallery ($slug): bool
    {

        $entity = entity($slug);

        return isset($entity['gallery']) && $entity['gallery'] == 1;

    }

    /**
     * Checks if a record can add files
     * @param  [type] $slug [description]
     * @return bool         [description]
     */
    function files ($slug): bool
    {

        $entity = entity($slug);

        return isset($entity['files']) && $entity['files'] == 1;

    }

    /**
     * Checks if a record can add photo
     * @param  [type] $slug [description]
     * @return bool         [description]
     */
    function photo ($slug): bool
    {

        $entity = entity($slug);

        return isset($entity['photo']) && $entity['photo'] == 0 ? false : true;

    }

    /**
     * Checks if import is possible
     * @param  [type] $slug [description]
     * @return bool         [description]
     */
    function import ($slug): bool
    {

        $entity = entity($slug);

        return isset($entity['import']) && $entity['import'] == 1;

    }

    /**
     * Checks if there is at least one field in the create section
     * @return bool [description]
     */
    function creatable ($slug): bool
    {
        $entity = entity($slug);

        if (isset($entity['create']) && $entity['create'] == false) {
            return false;
        }

        if (!isset($entity['fields']) || !is_array($entity['fields'])) {
            return false;
        }

        $fields = array_filter($entity['fields'], function($k) {
            return $k['create'] === 1;
        });

        return count($fields) != 0;

    }

    /**
     * Switch language in sting
     * @param  string $text [description]
     * @param  int $arrow [description]
     * @return string         [description]
     */
    function switcher ($text, $arrow=0)
    {
        $str[0] = array('й' => 'q', 'ц' => 'w', 'у' => 'e', 'к' => 'r', 'е' => 't', 'н' => 'y', 'г' => 'u', 'ш' => 'i', 'щ' => 'o', 'з' => 'p', 'х' => '[', 'ъ' => ']', 'ф' => 'a', 'ы' => 's', 'в' => 'd', 'а' => 'f', 'п' => 'g', 'р' => 'h', 'о' => 'j', 'л' => 'k', 'д' => 'l', 'ж' => ';', 'э' => '\'', 'я' => 'z', 'ч' => 'x', 'с' => 'c', 'м' => 'v', 'и' => 'b', 'т' => 'n', 'ь' => 'm', 'б' => ',', 'ю' => '.','Й' => 'Q', 'Ц' => 'W', 'У' => 'E', 'К' => 'R', 'Е' => 'T', 'Н' => 'Y', 'Г' => 'U', 'Ш' => 'I', 'Щ' => 'O', 'З' => 'P', 'Х' => '[', 'Ъ' => ']', 'Ф' => 'A', 'Ы' => 'S', 'В' => 'D', 'А' => 'F', 'П' => 'G', 'Р' => 'H', 'О' => 'J', 'Л' => 'K', 'Д' => 'L', 'Ж' => ';', 'Э' => '\'', '?' => 'Z', 'ч' => 'X', 'С' => 'C', 'М' => 'V', 'И' => 'B', 'Т' => 'N', 'Ь' => 'M', 'Б' => ',', 'Ю' => '.',);
        $str[1] = array (  'q' => 'й', 'w' => 'ц', 'e' => 'у', 'r' => 'к', 't' => 'е', 'y' => 'н', 'u' => 'г', 'i' => 'ш', 'o' => 'щ', 'p' => 'з', '[' => 'х', ']' => 'ъ', 'a' => 'ф', 's' => 'ы', 'd' => 'в', 'f' => 'а', 'g' => 'п', 'h' => 'р', 'j' => 'о', 'k' => 'л', 'l' => 'д', ';' => 'ж', '\'' => 'э', 'z' => 'я', 'x' => 'ч', 'c' => 'с', 'v' => 'м', 'b' => 'и', 'n' => 'т', 'm' => 'ь', ',' => 'б', '.' => 'ю','Q' => 'Й', 'W' => 'Ц', 'E' => 'У', 'R' => 'К', 'T' => 'Е', 'Y' => 'Н', 'U' => 'Г', 'I' => 'Ш', 'O' => 'Щ', 'P' => 'З', '[' => 'Х', ']' => 'Ъ', 'A' => 'Ф', 'S' => 'Ы', 'D' => 'В', 'F' => 'А', 'G' => 'П', 'H' => 'Р', 'J' => 'О', 'K' => 'Л', 'L' => 'Д', ';' => 'Ж', '\'' => 'Э', 'Z' => '?', 'X' => 'ч', 'C' => 'С', 'V' => 'М', 'B' => 'И', 'N' => 'Т', 'M' => 'Ь', ',' => 'Б', '.' => 'Ю', );
        return strtr($text,isset( $str[$arrow] )? $str[$arrow] :array_merge($str[0],$str[1]));
    }
