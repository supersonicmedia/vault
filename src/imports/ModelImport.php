<?php

namespace Supersonic\Vault\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Schema;

class ModelImport implements ToModel, WithValidation, WithHeadingRow, SkipsOnFailure, SkipsOnError
{
    use Importable, SkipsFailures, SkipsErrors;

    public function __construct(string $slug, array $validation_rules)
    {
        $this->slug = $slug;
        $this->validation_rules = $validation_rules;
    }

    public function model(array $row)
    {
        $entity = entity($this->slug);
        $classname = model($entity['name']);

        return new $classname($row);
    }

    public function rules(): array
    {
        return $this->validation_rules;
    }

}