<?php

namespace Supersonic\Vault\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationEmail extends Mailable
{
    use Queueable, SerializesModels;


    public $text;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($text)
    {
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $text = $this->text;
        //subject mail like 'Notification from the site Laravel'
        $subject = __('vault::messages.login') . ' ' . config('app.name');
        //mailing address
        $address = config('vault.notifications_email');

        return $this->from($address)->subject($subject)->view('vault::emails.notification', compact('text'));
    }
}