<?php

    /**
     * Disable website for strangers during development process
     * @var [type]
     */
    Route::match(['get', 'post'], '/hello', "Supersonic\Vault\Controllers\MainController@pass")->name("pass")->middleware('web');

    // Route::get('/', function () {
    //     //
    // })->middleware('pass');




	Route::group(['prefix' => config('vault.path'), 'as' => 'admin.'], function () {


        /**
         * Login & Logout
         * @var [type]
         */
		Route::group(['middleware' => 'web'], function () {
			Route::get('/login', 'Supersonic\Vault\Controllers\AuthController@login')->name('login');
			Route::post('/login', 'Supersonic\Vault\Controllers\AuthController@auth')->name('auth');
			Route::any('/logout', 'Supersonic\Vault\Controllers\AuthController@logout')->name('logout');
		});




    	/**
         * Admin panel pages
         * @var [type]
         */
		Route::group(['middleware' => ['web', 'admin']], function () {

			// Main page
			Route::get('/', 'Supersonic\Vault\Controllers\MainController@index')->name('home');

			// My profile
			Route::any('/help', 'Supersonic\Vault\Controllers\MainController@help')->name('help');

			// Resource
			Route::group(['as' => 'model.'], function () {
                Route::get('/{slug}/export', 'Supersonic\Vault\Controllers\ResourceController@export')->name('export');
                Route::post('/{slug}/import', 'Supersonic\Vault\Controllers\ResourceController@import')->name('import');
                Route::get('/{slug}', 'Supersonic\Vault\Controllers\ResourceController@index')->name('index');
                Route::get('/{slug}/create', 'Supersonic\Vault\Controllers\ResourceController@create')->name('create');
                Route::post('/{slug}/store', 'Supersonic\Vault\Controllers\ResourceController@store')->name('store');
                // Route::get('/{slug}/{id}', 'Supersonic\Vault\Controllers\ResourceController@show')->name('show');
                Route::post('/{slug}/actions', 'Supersonic\Vault\Controllers\ResourceController@actions')->name('actions');
                Route::get('/{slug}/{id}/edit', 'Supersonic\Vault\Controllers\ResourceController@edit')->name('edit');
                Route::post('/{slug}/{id}/update', 'Supersonic\Vault\Controllers\ResourceController@update')->name('update');
                Route::post('/{slug}/{id}/delete', 'Supersonic\Vault\Controllers\ResourceController@destroy')->name('delete');
            });

		});




    });
