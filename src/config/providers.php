<?php

    return [
    
        'admins' => [
            'driver' => 'eloquent',
            'model'  => 'Supersonic\Vault\Models\Admin',
        ],    

    ];