<?php

    return [
        
        'vault' => [
            'driver' => 'single',
            'path' => storage_path('logs/vault.log'),
        ],
        
    ];