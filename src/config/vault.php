<?php

    return [

        'path'                  => env('VAULT_PATH', 'vault'),
        'password'              => env('VAULT_PASSWORD', 'jagger'),
        'notifications_email'   => env('VAULT_NOTIFICATIONS_EMAIL', 'test@test.com'),
        'open_dev'              => env('VAULT_OPEN_DEV', false),
        'open_production'       => env('VAULT_OPEN_PRODUCTION', true),

        'models' => [
            'users' => [
                'name'                   => 'User',
                'title'                  => 'Пользователи',
                'fields'                 => [
                    'name'               => ['create' => 1, 'edit' => 1, 'show' => 1, 'type' => 'input', 'validate' => 'required|min:3'],
                    'password'           => ['create' => 1, 'edit' => 0, 'show' => 0, 'type' => 'password', 'validate' => 'required|string|min:6', 'validate_store' => ''],
                    'email'              => ['create' => 1, 'edit' => 1, 'show' => 1, 'type' => 'input', 'validate' => 'required|email|unique:users,email'],
                    'created_at'         => ['create' => 0, 'edit' => 0, 'show' => 1, 'type' => 'date'],
                    'is_admin'           => ['create' => 1, 'edit' => 1, 'show' => 1, 'type' => 'checkbox'],
                    'notifications_on'   => ['create' => 1, 'edit' => 1, 'show' => 1, 'type' => 'checkbox'],
                ],
            ],
            'settings' => [
                'name'                   => 'Setting',
                'title'                  => 'Настройки',
                'photo'                  => 0,
                'fields'                 => [
                    'property'           => ['create' => 0, 'edit' => 0, 'show' => 1, 'type' => 'input'],
                    'value'              => ['create' => 1, 'edit' => 1, 'show' => 1, 'type' => 'input', 'validate' => 'required'],
                ],
            ],
            'metrics' => [
                'name'                   => 'Metric',
                'title'                  => 'Метрики',
                'photo'                  => 0,
                'fields'                 => [
                    'property'           => ['create' => 0, 'edit' => 0, 'show' => 1, 'type' => 'input'],
                    'value'              => ['create' => 1, 'edit' => 1, 'show' => 1, 'type' => 'input', 'validate' => 'required'],
                ],
            ],
            'menu' => [
                'name'                   => 'Menu',
                'title'                  => 'Меню',
                'photo'                  => 0,
                'fields'                 => [
                    'title'              => ['create' => 1, 'edit' => 1, 'show' => 1, 'type' => 'input', 'validate' => 'required'],
                    'link'               => ['create' => 1, 'edit' => 1, 'show' => 1, 'type' => 'input', 'validate' => 'required'],
                    'parent_id'          => ['create' => 1, 'edit' => 1, 'show' => 1, 'type' => 'select', 'model' => 'Menu', 'field' => 'title'],
                    'is_blank'           => ['create' => 1, 'edit' => 1, 'show' => 0, 'type' => 'checkbox'],
                    'sorting_order'      => ['create' => 1, 'edit' => 1, 'show' => 0, 'type' => 'input', 'validate' => 'nullable|numeric'],
                ],
            ],
        ],

        'guards' => [
            'admin' => [
                'driver'   => 'session',
                'provider' => 'users',
            ],
        ],

    ];
