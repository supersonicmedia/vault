<?php

namespace Supersonic\Vault\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Schema;

class ModelExport implements FromCollection, WithHeadings
{
    public function __construct(string $slug)
    {
        $this->slug = $slug;
    }
    
    public function headings(): array
    {
        $entity = entity($this->slug);
        $classname = model($entity['name']);
        $item = $classname::first();

        if ($item) {
            $columns = Schema::getColumnListing($item->getTable());
            return $columns;
        } else {
            return [];
        }
    }
    
    public function collection ()
    {
        $entity = entity($this->slug);
        $classname = model($entity['name']);
        return $classname::all();
    }
}