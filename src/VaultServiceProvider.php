<?php

namespace Supersonic\Vault;

use Illuminate\Support\ServiceProvider;

class VaultServiceProvider extends ServiceProvider
{
    /**
    * Bootstrap services.
    *
    * @return void
    */
    public function boot()
    {
        require_once __DIR__.'/helpers.php';
        include __DIR__.'/routes.php';
        
        $this->publishes([__DIR__.'/config/vault.php' => config_path('vault.php'),]);

        // Icons for trumbowyg
        $this->publishes([__DIR__.'/img/trumbowyg-icons.svg' => public_path('img/trumbowyg-icons.svg'),]);

        // Bulma
        $this->publishes([__DIR__.'/bulma/bulma.min.css' => public_path('css/bulma.min.css'),]);
        $this->publishes([__DIR__.'/bulma/all.js' => public_path('js/all.js'),]);

        // Loop for create migrations
        $migrations = [
            'AddIsUser' => 'add_is_user',
            'CreateSettings' => 'create_settings',
            'AddNotificationsOn' => 'add_notifications_on',
            'CreateMenu' => 'create_menu',
        ];
        foreach ($migrations as $class_migration => $migration_name) {
            if (!class_exists($class_migration)) {
                $this->publishes([
                    __DIR__.'/migrations/' . $migration_name . '.php.stub' => database_path('migrations/'.date('Y_m_d_His', time()).'_' . $migration_name . '.php'),
                ], 'migrations');
            }
        }

        // Seeders
        $this->publishes([__DIR__.'/seeds/AdminSeeder.php.stub' => database_path('seeds/AdminSeeder.php'),]);
        $this->publishes([__DIR__.'/seeds/SettingsSeeder.php.stub' => database_path('seeds/SettingsSeeder.php'),]);

        $router = $this->app['router'];

        // Admin login middleware
        $router->pushMiddlewareToGroup('admin', Middleware\AuthenticateAdmin::class);
        $router->pushMiddlewareToGroup('login', Middleware\Login::class);

        // Disable website for strangers during development process
        $router->pushMiddlewareToGroup('pass', Middleware\Pass::class);

        $this->loadTranslationsFrom(__DIR__ . '/lang', 'vault');
        
    }

    /**
    * Register services.
    *
    * @return void
    */
    public function register() {

        $this->app->make('Supersonic\Vault\Controllers\AuthController');
        $this->app->make('Supersonic\Vault\Controllers\MainController');
        $this->app->make('Supersonic\Vault\Controllers\ResourceController');

        $this->loadViewsFrom(__DIR__ . '/views', 'vault');

        $this->mergeConfigFrom(__DIR__   . '/config/providers.php', 'auth.providers');
        $this->mergeConfigFrom(__DIR__   . '/config/guards.php', 'auth.guards');
        $this->mergeConfigFrom(__DIR__   . '/config/logging.php', 'logging.channels');
        $this->mergeConfigFrom(__DIR__   . '/config/app.php', 'app');
    }

}
