<?php

namespace Supersonic\Vault\Middleware;

use Closure;

class Pass
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $open_dev = config('vault.open_dev') ?? false;
        $open_production = config('vault.open_production') ?? true;

        if (app()->environment('production') && $open_production) return $next($request);
        if (app()->environment('dev') && $open_dev) return $next($request);
        if (app()->environment('local')) return $next($request);

        if (session('pass')) {
            return $next($request);
        } else {
            return redirect()->route('pass');
        }
    }
}
