<?php

    return [
        'add'               => 'Add',
        'password'          => 'Password',
        'login'             => 'Login',
        'profile'           => 'Profile',
        'help'              => 'Help',
        'logout'            => 'Logout',
        'previous'          => 'Previous',
        'next'              => 'Next',
        'view'              => 'View',
        'edit'              => 'Edit',
        'create'            => 'Create',
        'delete'            => 'Delete',
        'general'           => 'General',
        'other'             => 'Other',
        'notification'      => 'Notification',
        'notification_from' => 'Notification from the site',
        'empty'             => 'Empty',
        'content'           => 'Content',
        'dashboard'         => 'Dashboard',
        'confirm'           => 'Are you sure?',
        'quantity'          => 'Quantity',
        'open-website'      => 'Open website',
        'back'              => 'Back',
        'save'              => 'Save',
        'choose-file'       => 'Choose a file',
        'choose-files'      => 'Choose a file(s)',
        'selected-files'    => 'files selected',
        'delete-image'      => 'Delete image',
        'delete-file'       => 'Delete file',
        'hi'                => 'Hi',
        'created'           => 'Created',
        'opened'            => 'Opened',
        'updated'           => 'Updated',
        'deleted'           => 'Deleted',
        'user'              => 'User',
        'hours'             => 'Hours',
        'export_xls'        => 'Export',
        'import_xls'        => 'Import',
        'files_restriction' => 'Maximum size – 5 MB, minimum width and height – 100х100. Formats: jpeg, png, bmp, gif, or svg.',
        'sort'              => 'Order by',
        'asc'               => 'Ascending',
        'desc'              => 'Descending',
        'title-gallery'     => 'Gallery',
        'title-files'       => 'Files',
        'settings'          => 'Settings',
        'validate_error'    => 'Incorrectly filled fields',
        'success'           => 'Action completed successfully',
        'warning'           => 'Action not fully completed',
        'error'             => 'An error occurred during execution',
        'error_param'       => 'An error occurred during execution. Error: :param',
        'warning_param'     => 'Attention! :param',
        'success_param'     => 'Success! :param',
        'warning_import'    => 'When importing, :skip rows were skipped due to incorrect data.',
        'warning_files'     => 'When adding, :skip files were skipped due to incorrect data.',
        'help_nothing'      => 'Nothing found',
        'help_request'      => 'Enter your request',
        'help_error'        => 'An error has occurred',
        'just_typing'       => 'Just start typing',
        'to_site'           => 'Go to the website',
        'group'             => 'Other',
        'horizontal'        => 'Horizontal',
        'vertical'          => 'Vertical',
        'metrics'           => 'Metrics',
        'actions'           => 'You must select entries',
        'activate'          => 'Activate',
        'deactivate'        => 'Deactivate',
        'delete'            => 'Delete',
    ];
