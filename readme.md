CSS Framework:
https://bulma.io

Install project: `composer create-project --prefer-dist laravel/laravel screen`, `cd screen`

Add repo and install the package:

```
composer config repositories.foo vcs https://name@bitbucket.org/supersonicmedia/vault.git && composer require supersonic/vault
```

Publish files (config)
`php artisan vendor:publish --provider="Supersonic\Vault\VaultServiceProvider"`

Publish files from dependence

`php artisan vendor:publish --provider="Spatie\MediaLibrary\MediaLibraryServiceProvider" --tag="migrations"`

`php artisan vendor:publish --provider="Spatie\MediaLibrary\MediaLibraryServiceProvider" --tag="config"`

Migrate
`php artisan migrate`

Seed

`php artisan db:seed --class=Supersonic\\Vault\\Seeds\\AdminSeeder`

`php artisan db:seed --class=Supersonic\\Vault\\Seeds\\SettingsSeeder`

In case of removal
`composer remove supersonic/vault --update-with-dependencies`